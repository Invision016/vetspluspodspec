Pod::Spec.new do |s|  
    s.name              = 'VetsPlus'
    s.version           = '2.0.1'
    s.summary           = 'Private framework for VetsPlus client'
    s.homepage          = 'http://example.com/'

    s.author            = { 'Name' => 'Invision' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :http => 'https://firebasestorage.googleapis.com/v0/b/vetsdoctor-261e5.appspot.com/o/VetsPlus_2.0.1.zip?alt=media&token=2507f530-1072-4e6c-8c3c-6055f24b81aa' }
    s.swift_version = '5'
    s.ios.deployment_target = '12.0'
    s.ios.vendored_frameworks = 'VetsPlus.framework'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.dependency 'MessageKit'
    s.dependency 'Socket.IO-Client-Swift'
    s.dependency 'OpenTok'
    s.dependency 'Floaty'
    s.dependency 'TwilioChatClient'
    s.dependency 'TwilioVideo'
    s.dependency 'SkeletonView'
end  